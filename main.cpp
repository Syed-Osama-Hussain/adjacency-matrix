#include <iostream>
#include <string>
#include "graph.h"
using namespace std;


int main()
{
  graph g1;
  g1.add_element(0);
  g1.add_element(1);
  g1.add_element(2);
  g1.add_element(3);
  g1.add_element(4);
  g1.add_element(5);
  g1.add_neighbour(0,1);
  g1.add_neighbour(0,2);
  g1.add_neighbour(1,0);
  g1.add_neighbour(1,2);
  g1.add_neighbour(2,0);
  g1.add_neighbour(2,1);
  g1.add_neighbour(2,3);
  g1.add_neighbour(3,2);
  g1.add_neighbour(4,2);
  g1.add_neighbour(5,0);
  g1.add_neighbour(5,4);
  g1.display();
  system("pause");

}